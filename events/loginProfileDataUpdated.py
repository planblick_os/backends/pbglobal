from .abstract import Event
import datetime
import hashlib

class loginProfileDataUpdated(Event):
    def __init__(self, consumer_id=None, login=None, data=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner_id = consumer_id
        self.login = login
        self.data = data
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        self.internal_validate()
        assert self.owner_id is not None, "owner_id may not be empty"
        assert self.login is not None, "login may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.data is not None, "data may not be empty"
        assert self.data is not dict, "data must be an object"
        #assert self.data.get("email") is not None, "data.email must be set"
        assert self.data.get("lastname") is not None, "data.lastname must be set"
        assert self.data.get("firstname") is not None, "data.firstname must be set"

        return True
