from .abstract import Event


class qrCodeCreated(Event):
    def __init__(self, name=None, data=None, consumer_id=None, correlation_id=None, skip_validation=False):
        super().__init__()
        self.name = name
        self.data = data
        self.consumer_id = consumer_id
        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.name is not None, "name may not be empty"
        assert self.data is not None, "data may not be empty"
        assert self.consumer_id is not None, "consumer_id may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
