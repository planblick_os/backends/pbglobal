import json
from pbglobal.pblib.amqp import client

class Event():
    def __init__(self, amqp_client=client, skip_validation=False):
        self.amqp_client = amqp_client
        self.kind = "event"
        self.owner = None
        self.creator = None
        self.correlation_id = None
        self.create_time = None

    def __repr__(self):
        return self.to_string()

    def validate(self):
        return dict(result=False, error="validate-function not implemented")

    def internal_validate(self):
        assert self.kind == "event", "kind must be event"
        assert self.owner is not None, "owner may not be empty, use unknown if not available"
        assert self.creator is not None, "creator may not be empty, use unknown if not available"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        
        return dict(result=True)

    def to_dict(self):
        items = vars(self)
        if "amqp_client" in items:
            del items["amqp_client"]
        return items

    def to_string(self):
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(event, data, skip_validation = False):
        instance = event(skip_validation=True)
        for member in instance.__dict__:
            if member != "kind":
                setattr(instance, member, data.get(member))
        if skip_validation is False:
            instance.validate()
            instance.internal_validate()

        return instance

    def publish(self):
        try:
            self.validate()
            self.internal_validate()
            amqp = client()
            amqp.publish("message_handler", self.__class__.__name__, self.to_string())
            return True
        except Exception as e:
            print(e)
            return False

