from .abstract import Command


class updateContactConfiguration(Command):
    def __init__(self, consumer_id=None, contact_configuration_id=None, data=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.consumer_id = consumer_id
        self.contact_configuration_id = contact_configuration_id
        self.data = data
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.consumer_id is not None, "consumer_id may not be empty"
        assert self.contact_configuration_id is not None, "contact_configuration_id may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.data is not None, "data may not be empty"
        assert self.data is not dict, "data must be an object"

        return True
