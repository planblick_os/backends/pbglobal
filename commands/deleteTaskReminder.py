from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt
import uuid

class deleteTaskReminder(Command):
    def __init__(self, owner_id=None, creator=None, reminder_id=None, correlation_id=None, create_time=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.creator = creator
        self.create_time = create_time
        self.reminder_id = reminder_id
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.reminder_id is not None and len(self.reminder_id) > 0, "reminder_id may not be empty"

        return True
