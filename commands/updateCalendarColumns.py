from .abstract import Command


class updateCalendarColumns(Command):
    def __init__(self,
                 data=None,
                 correlation_id=None,
                 owner=None,
                 creator=None,
                 skip_validation=False
                 ):
        super().__init__()
        self.data = data,

        if not skip_validation:
            self.validate()


    def validate(self):
        assert self.data is not None, "data may not be empty"

        return True
