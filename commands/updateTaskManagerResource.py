from .abstract import Command

from pbglobal.pblib.pbrsa import encrypt


class updateTaskManagerResource(Command):
    def __init__(self, owner_id=None, type="room", resource_id=None, resource_data=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.type = type
        self.resource_data = resource_data
        self.resource_id = resource_id
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "login may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.resource_id is not None and len(self.resource_id) > 0, "resource_id may not be empty"
        assert self.resource_data is not None, "resource_data may not be empty"
        assert self.resource_data is not dict, "resource_data must be an object"
        assert self.resource_data.get("name") is not None, "self.resource_data.name must be set"

        return True