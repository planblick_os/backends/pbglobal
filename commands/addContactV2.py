from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt

class addContactV2(Command):
    def __init__(self, owner=None, id=None, hash=None, addedtime=None, external_id=None, correlation_id=None, skip_validation=False):
        super().__init__()

        self.owner = owner
        self.id = id
        self.hash = hash
        self.addedtime = addedtime
        self.external_id = external_id

        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "owner may not be empty"
        assert self.hash is not None, "hash may not be empty"
        assert self.id is not None, "id may not be empty"
        assert self.addedtime is not None, "addedtime may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"

        return True
