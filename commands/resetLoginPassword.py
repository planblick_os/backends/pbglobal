from .abstract import Command
from pbglobal.pblib.helpers import generate_password

class resetLoginPassword(Command):
    def __init__(self, username=None, consumer_id=None, password=None, correlation_id=None, skip_validation=False):
        super().__init__()
        self.username = username
        self.password = password if password is not None else "tmp_" + generate_password(15)
        self.consumer_id = consumer_id
        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.username is not None, "username may not be empty"
        assert self.password is not None, "password may not be empty"
        assert self.consumer_id is not None, "consumer_id may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
