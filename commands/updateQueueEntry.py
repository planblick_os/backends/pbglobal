from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt
import uuid

class updateQueueEntry(Command):
    def __init__(self, owner_id=None, entry_id=None, entry_data=None, correlation_id=None, links=[],  skip_validation=False, form_data=None, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.entry_data = entry_data
        self.entry_id = entry_id
        self.correlation_id = correlation_id
        self.links = links

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "login may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.entry_id is not None and len(self.entry_id) > 0, "entry_id may not be empty"
        assert self.entry_data is not None, "entry_data may not be empty"
        assert self.entry_data is not dict, "entry_data must be an object"
        assert self.entry_data.get("title") is not None, "self.entry_data.title must be set"

        return True
