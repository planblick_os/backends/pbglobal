from cryptography.fernet import Fernet

class PbCrypt():
    def __init__(self, key = None):
        self.key = key
        if self.key != "disabled":
            self.f = Fernet(self.key)

    def encrypt(self, string):
        if self.key != "disabled":
            return self.f.encrypt(string)
        else:
            return string

    def decrypt(self, string):
        if self.key != "disabled":
            return self.f.decrypt(string)
        else:
            return string

    @staticmethod
    def generate_key(self):
        return Fernet.generate_key()