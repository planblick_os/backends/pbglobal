from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import copy

class PbDateCalc():
    def __init__(self, diffset, basedate=datetime.utcnow()):
        self.basedate = basedate
        self.result_date = self.basedate
        self.fixed_time = diffset.get("fixed_time")
        del(diffset["fixed_time"])
        self.diffset = diffset

        self.key_map_dict = {
            "i": "minutes",
            "h": "hours",
            "d": "days",
            "w": "weeks",
            "m": "months",
            "y": "years"
        }
        pass

    def convert_diffkeys_to_python(self):
        self.diffset = {self.key_map_dict.get(k, k): v for k, v in self.diffset.items()}

    def split_diffset_by_needed_function(self):
        delta_params = copy.copy(self.diffset)
        relativedelta_params = copy.copy(self.diffset)
        del(delta_params["months"])
        del(delta_params["years"])
        del(relativedelta_params["minutes"])
        del(relativedelta_params["hours"])
        del(relativedelta_params["days"])
        del(relativedelta_params["weeks"])
        return (delta_params, relativedelta_params)

    def get_calculated_date(self):
        self.convert_diffkeys_to_python()
        delta_params, relative_delta_params = self.split_diffset_by_needed_function()

        self.result_date = self.result_date + timedelta(**delta_params)
        self.result_date = self.result_date + relativedelta(**relative_delta_params)

        if self.fixed_time:
            hour, minute = self.fixed_time.split(":")
            self.result_date = self.result_date.replace(hour=int(hour), minute=int(minute))

        return self.result_date



# diffset = {
#     "i": 10,
#     "h": 2,
#     "d": 1,
#     "w": 0,
#     "m": 1,
#     "y": 1
#  }
#print(PbDateCalc(diffset).get_calculated_date())

